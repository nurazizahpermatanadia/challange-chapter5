
const game = () => {
    const play = () => {
      const rockPlayerBtn = document.querySelector(".rock-p");
      const paperPlayerBtn = document.querySelector(".paper-p");
      const scissorPlayerBtn = document.querySelector(".scissor-p");
      const rockComBtn = document.querySelector(".rock-c");
      const paperComBtn = document.querySelector(".paper-c");
      const scissorComBtn = document.querySelector(".scissor-c");
      const playerChoices = [rockPlayerBtn, paperPlayerBtn, scissorPlayerBtn];
      const comChoices = [rockComBtn,paperComBtn,scissorComBtn,
      ];
  
      playerChoices.forEach((option) => {
        option.addEventListener("click", () => {
          let comChoice = comChoices[Math.floor(Math.random() * 3)];
          let comBtnGroup = document.querySelector(".com-button-group");
          console.log(option);
          console.log(comChoice);
          winner(option.id, comChoice.id);
        });
      });
    };

    const winner = (player, com) => {
      let result = document.querySelector(".container-result");
      let comChoose = document.querySelector(".select-com");
      let playerChoose = document.querySelector(".player-select");
      if (player === com) {
        result.innerHTML = `<div
        class="d-flex container-result-draw justify-content-center align-items-center"
      >
        <h4 class="result text-center">OMG Draw!</h4>
      </div>`;
      
        comChoose.innerHTML = `Com: ${com}`;
        playerChoose.innerHTML = `Player: ${player}`;
        console.log("draw");
      } else if (
        (player === "rock" && com === "papper") ||
        (player === "scissor" && com === "paper") ||
        (player === "papper" && com === "rock")
      ) {
        result.innerHTML = `<div
        class="d-flex container-result-win justify-content-center align-items-center"
      >
        <h4 class="result text-center">Yeah You Win!</h4>
      </div>`;
        comChoose.innerHTML = `Com: ${com}`;
        playerChoose.innerHTML = `Player: ${player}`;
        console.log("win");
      } else {
        result.innerHTML = `<div
        class="d-flex container-result-lose justify-content-center align-items-center"
      >
        <h4 class="result text-center">Oh You Lose!</h4>
      </div>`;
        comChoose.innerHTML = `Com: ${com}`;
        playerChoose.innerHTML = `Player: ${player}`;
        console.log("lose");
      }
    };
  
    const reloadBtn = document.querySelector(".refresh");
    reloadBtn.addEventListener("click", () => {
      window.location.reload();
    });
  
    play();
  };
  
  game();
  
  